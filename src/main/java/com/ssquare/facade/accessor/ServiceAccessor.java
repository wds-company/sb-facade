package com.ssquare.facade.accessor;

import javax.servlet.http.HttpServletRequest;

import com.ssquare.facade.common.ServiceCommon;
import com.ssquare.facade.common.context.Context;
import com.ssquare.facade.common.log.Logging;
import com.ssquare.facade.common.model.RequestData;

/**
 * The super class of accessor
 * 
 * @author adirak-vdi
 *
 */
public final class ServiceAccessor {

	/**
	 * Create instance with request
	 * 
	 * @param request
	 */
	public ServiceAccessor(HttpServletRequest request) {
		context = new Context(request);
	}

	/**
	 * Create instance with request
	 * 
	 * @param request
	 * @param response
	 */
	public ServiceAccessor(HttpServletRequest request, RequestData requestData) {
		context = new Context(request, requestData);
	}

	// Member
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++

	/** instance of context */
	public final Context context;

	/** instance of service common */
	public final ServiceCommon common = ServiceCommon.common;

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++

	// Method function
	// *****************************************************

	/**
	 * Get logging from class
	 * 
	 * @return logging
	 */
	public static Logging getLogging() {
		return Logging.logging;
	}

	/**
	 * Throw exception
	 * 
	 * @param ex
	 */
	public void throwException(Exception ex) {
		String msg = ex.getMessage();
		if (msg == null || msg.trim().isEmpty()) {
			StackTraceElement[] st = ex.getStackTrace();
			StringBuilder sb = new StringBuilder();
			for(StackTraceElement e : st) {
				sb.append(e.toString());
				sb.append("\n");
			}
			msg = sb.toString();
		}
		context.response.setErrorMessage(ex.getMessage());
	}

	/**
	 * Throw exception with code and message
	 * 
	 * @param ex
	 */
	public void throwException(String code, String message) {
		context.response.setMessageData(code, message);
	}

	/**
	 * Return success message
	 * @param message
	 */
	public void returnSuccess(String message) {
		context.response.setSuccessfulMessage(message);
	}

	/**
	 * Return success data
	 */
	public void returnSuccess() {
		context.response.setSuccessful();
	}

	// *****************************************************
}
