package com.ssquare.facade.common;

import java.io.IOException;

import com.ssquare.facade.common.config.DefaultConfig;
import com.ssquare.facade.common.config.PropConfig;
import com.ssquare.facade.common.file.File;
import com.ssquare.facade.common.handler.Bean;
import com.ssquare.facade.common.handler.SSquare;
import com.ssquare.facade.common.handler.number.Number;
import com.ssquare.facade.common.json.Json;
import com.ssquare.facade.common.log.Logging;

/**
 * The common of service
 * 
 * @author adirak-vdi
 *
 */
public final class ServiceCommon {

	/**
	 * Don't create new instance
	 */
	private ServiceCommon() {
	}

	/** instance of this class */
	public static final ServiceCommon common = new ServiceCommon();

	/** instance of ssquare */
	public final SSquare ssquare = SSquare.ssquare;

	/** instance of file */
	public final File file = File.file;

	/** instance of logging */
	public final Logging logging = Logging.logging;

	/** instance of json */
	public final Json json = Json.json;
	
	/** instance of number */
	public final Number number = Number.number;
	
	/** instance of bean */
	public final Bean bean = Bean.bean;

	// Methods
	// *******************************************

	/**
	 * Get property by default path
	 * 
	 * @return prop
	 * @throws IOException
	 */
	public PropConfig getProp() throws IOException {
		return PropConfig.getProp(new java.io.File(DefaultConfig.getConfigPath(DefaultConfig.CONF_DIR, DefaultConfig.CONF_FILE)), DefaultConfig.CHARSET);
	}

	/**
	 * Get property by file
	 * 
	 * @return prop
	 * @throws IOException
	 */
	public PropConfig getProp(java.io.File file, String charset) throws IOException {
		return PropConfig.getProp(file, charset);
	}
	
	/**
	 * Get property by properties string
	 * 
	 * @return prop
	 * @throws IOException
	 */
	public PropConfig getProp(String stringProp, String charset) throws IOException {
		return PropConfig.getProp(stringProp, charset);
	}

	// *******************************************

}
