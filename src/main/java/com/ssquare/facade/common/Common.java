package com.ssquare.facade.common;

/**
 * For making class bundle common tool
 * @author adirak-vdi
 * 
 */
public abstract class Common {

	/** instance of service common */
	public static final ServiceCommon common = ServiceCommon.common;

}
