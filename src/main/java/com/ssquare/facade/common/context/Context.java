package com.ssquare.facade.common.context;

import javax.servlet.http.HttpServletRequest;

import com.ssquare.facade.common.model.RequestData;
import com.ssquare.facade.common.model.ResponseData;

public final class Context {

	public Context(HttpServletRequest request) {
		this.httpRequest = request;
		this.request = new RequestData();
	}
	
	public Context(HttpServletRequest request, RequestData requestData) {
		this.httpRequest = request;
		this.request = requestData;
	}
	
	
	/** Request Data instance*/
	public final HttpServletRequest httpRequest;
	public final RequestData request;
	
	/** Response Data instance */
	public final ResponseData response = new ResponseData();

}
