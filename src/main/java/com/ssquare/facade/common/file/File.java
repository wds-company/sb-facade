package com.ssquare.facade.common.file;

import com.ssquare.facade.common.file.reader.FileReader;

/**
 * 
 * @author adirak-vdi
 *
 */
public final class File {

	/** Don't create new instance */
	private File() {
	}
	
	/** instance of this class */
	public static final File file = new File();
	
	/** instance of file reader */
	public final FileReader reader = FileReader.reader;

}
