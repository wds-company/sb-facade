package com.ssquare.facade.common.file.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;

import com.ssquare.facade.common.config.DefaultConfig;

public final class FileReader {

	public static final FileReader reader = new FileReader();

	/** Don't create new instance */
	private FileReader() {
	}

	/**
	 * Read file to string
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public String readFileToString(File file) throws IOException {
		return FileUtils.readFileToString(file, DefaultConfig.CHARSET);
	}

	/**
	 * Read file to string
	 * 
	 * @param file
	 * @param charset
	 * @return
	 * @throws IOException
	 */
	public String readFileToString(File file, String charset) throws IOException {
		return FileUtils.readFileToString(file, charset);
	}

	/**
	 * Read resource to string, you can use this function to read file in your jar
	 * 
	 * @param resourcePath
	 * @return
	 * @throws IOException
	 */
	public String readResourceToString(String resourcePath) throws IOException {
		return readResourceToString(resourcePath, DefaultConfig.CHARSET);
	}

	/**
	 * Read resource to string, you can use this function to read file in your jar
	 * 
	 * @param resourcePath
	 * @param charset
	 * @return
	 * @throws IOException
	 */
	public String readResourceToString(String resourcePath, String charset) throws IOException {

		InputStream is = null;
		BufferedReader br = null;
		String line;

		StringBuilder sb = new StringBuilder();

		try {
			is = FileUtils.class.getResourceAsStream(resourcePath);
			br = new BufferedReader(new InputStreamReader(is, charset));
			int rowCount = 0;
			while (null != (line = br.readLine())) {
				if (rowCount > 0) {
					sb.append("\n");
				}
				sb.append(line);
				rowCount++;
			}
		} catch (Exception e) {
			throw new IOException(e);
		} finally {

			try {
				if (br != null)
					br.close();
				if (is != null)
					is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
