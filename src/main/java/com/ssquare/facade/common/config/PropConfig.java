package com.ssquare.facade.common.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class to read property file
 * 
 * @author adirak-vdi
 *
 */
public final class PropConfig {

	/** Don't create new instance */
	private PropConfig() {
	}

	/** property */
	private java.util.Properties prop;

	/**
	 * Constructor to create Property
	 * 
	 * @param file
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private PropConfig(File file, String charset) throws IOException {
		this(new FileInputStream(file), charset);
	}

	/**
	 * Constructor to create Property
	 * 
	 * @param propString
	 * @param charset
	 * @throws IOException
	 */
	private PropConfig(String propString, String charset) throws IOException {
		this(new ByteArrayInputStream(propString.getBytes(Charset.forName(charset))), charset);
	}

	/**
	 * Constructor to create Property
	 * 
	 * @param is
	 * @param charset
	 * @throws IOException
	 */
	private PropConfig(InputStream is, String charset) throws IOException {
		prop = new java.util.Properties();
		InputStreamReader reader = new InputStreamReader(is, charset);
		prop.load(reader);
	}

	/**
	 * Create property by file
	 * 
	 * @param file
	 * @param charset
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static PropConfig getProp(File file, String charset) throws FileNotFoundException, IOException {
		return new PropConfig(file, charset);
	}

	/**
	 * Create property by string data
	 * 
	 * @param propString
	 * @param charset
	 * @return
	 * @throws IOException
	 */
	public static PropConfig getProp(String propString, String charset) throws IOException {
		return new PropConfig(propString, charset);
	}

	/**
	 * Create property by inputstream
	 * 
	 * @param is
	 * @param charset
	 * @return
	 * @throws IOException
	 */
	public static PropConfig getProp(InputStream is, String charset) throws IOException {
		return new PropConfig(is, charset);
	}

	/**
	 * Get value of property
	 * 
	 * @param name
	 * @return
	 */
	public String getValue(String name) {
		if (prop == null) {
			throw new RuntimeException("prop is null!");
		}

		String value = prop.getProperty(name);

		// Loop to find reference value ${xxxxxx} in properties file
		if (value != null && value.contains("${")) {
			int s = value.indexOf("${");
			while (s >= 0) {
				
				int e = value.indexOf("}", s + 2);
				
				if (e > s) {

					String paramF = value.substring(s, e + 1);

					int ss = s + 2;
					int ee = e;
					String paramS = value.substring(ss, ee);
					String valueS = getValue(paramS);

					if (valueS == null) {
						valueS = "undefined";
					}

					value = value.replace(paramF, valueS);

					s = value.indexOf("${");
					
				} else {
					break;
				}

			}
		}

		return value;
	}

	/**
	 * List the name of field in properties
	 * 
	 * @return
	 */
	public List<String> listName() {
		Iterator<Object> iter = prop.keySet().iterator();
		List<String> names = new ArrayList<String>();
		while (iter.hasNext()) {
			Object key = iter.next();
			String name = key.toString();
			names.add(name);
		}
		return names;
	}

	/**
	 * Run Testing
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		String path = "D:\\Developing-2020-BitBucket\\sb-flow\\conf\\nodeStyle.prop";
		PropConfig pc = new PropConfig(new File(path), "UTF-8");

		String value = pc.getValue("MULTIDECISION");
		System.out.println(value);

	}

}
