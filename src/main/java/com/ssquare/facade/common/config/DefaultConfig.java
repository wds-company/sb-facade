package com.ssquare.facade.common.config;

public final class DefaultConfig {

	/** Don't create instance */
	private DefaultConfig() {
	}

	// Default Charset of the app
	public static String CHARSET = "UTF-8";

	/** Config path of app */
	public static String CONF_DIR = "./conf";

	/** Config file name of app */
	public static String CONF_FILE = "module.conf";

	/**
	 * Get config file path
	 * @param dir
	 * @param fileName
	 * @return
	 */
	public static String getConfigPath(String dir, String fileName) {
		String fullPath = CONF_DIR + "/" + CONF_FILE;
		return fullPath;
	}

}
