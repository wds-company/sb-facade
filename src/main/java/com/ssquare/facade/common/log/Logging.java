package com.ssquare.facade.common.log;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class Logging {

	/**
	 * Don't create instance
	 */
	private Logging() {
	}

	/** Instance of this class */
	public static final Logging logging = new Logging();

	/** Log buffer */
	public static Map<String, Log> loggers = new HashMap<String, Log>();

	/**
	 * Get logger by class
	 * 
	 * @param theClass
	 * @return
	 */
	public Log getLogger(Class<?> ownerClass) {
		String name = ownerClass.getName();
		Log log = loggers.get(name);
		if (log == null) {
			log = LogFactory.getLog(ownerClass);
			loggers.put(name, log);
		}
		return log;
	}

	/**
	 * Get logger by object
	 * 
	 * @param ownerObject
	 * @return
	 */
	public Log getLogger(Object ownerObject) {
		return this.getLogger(ownerObject.getClass());
	}

}
