package com.ssquare.facade.common.handler.ssquare.data;

import java.util.HashMap;
import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.Flow;

import techberry.ewi.graph.editor.EWICell;

public final class NodeCell {

	public final long flowId;
	public final String nodeId;
	public final Flow flow;
	public final EWICell cell;
	public final Map<String, Object> props;

	/**
	 * 
	 * @param flowId
	 * @param nodeId
	 * @param flow
	 * @param cell
	 * @param props
	 */
	private NodeCell(long flowId, String nodeId, Flow flow, EWICell cell, Map<String, Object> props) {
		this.flowId = flowId;
		this.nodeId = nodeId;
		this.flow = flow;
		this.cell = cell;

		if (props == null) {
			props = new HashMap<String, Object>();
		}
		this.props = props;
	}

	/**
	 * Create instance of node cell
	 * 
	 * @param flowId
	 * @param nodeId
	 * @param flow
	 * @param cell
	 * @return nodeCell
	 */
	public static NodeCell create(long flowId, String nodeId, Flow flow, EWICell cell) {
		return new NodeCell(flowId, nodeId, flow, cell, new HashMap<String, Object>());
	}

	/**
	 * Create instance of node cell
	 * 
	 * @param flowId
	 * @param nodeId
	 * @param flow
	 * @param cell
	 * @param props
	 * @return nodeCell
	 */
	public static NodeCell create(long flowId, String nodeId, Flow flow, EWICell cell, Map<String, Object> props) {
		return new NodeCell(flowId, nodeId, flow, cell, props);
	}

	/**
	 * To merge props data
	 * 
	 * @param props
	 */
	public void setProps(Map<String, Object> props) {
		if (props != null) {
			this.props.putAll(props);
		}
	}

	/**
	 * To set prop by name
	 * 
	 * @param name
	 * @param value
	 */
	public void setProp(String name, Object value) {
		this.props.put(name, value);
	}

	/**
	 * To get prop by name
	 * 
	 * @param name
	 * @return value
	 */
	public Object getProp(String name) {
		return props.get(name);
	}

	/**
	 * To remove prop
	 * 
	 * @param name
	 */
	public void delProp(String name) {
		props.remove(name);
	}

}
