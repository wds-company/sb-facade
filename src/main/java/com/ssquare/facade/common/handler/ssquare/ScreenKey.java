package com.ssquare.facade.common.handler.ssquare;

import techberry.ewi.graph.editor.EWIKeypadCellDialog;

public final class ScreenKey {

	/** Don't create instance */
	private ScreenKey() {
	}

	/** instance of this class */
	public final static ScreenKey screenKey = new ScreenKey();

	/**
	 * Get execute code by key name
	 * @param keyName
	 * @return
	 */
	public String getKeyExecuteCode(String keyName) {
		return EWIKeypadCellDialog.getKeyValue(keyName);
	}
	
	/**
	 * Get key name from execute code
	 * @param executeCode
	 * @return
	 */
	public String getKeyFromExecuteCode(String executeCode) {
		return EWIKeypadCellDialog.getKeyName(executeCode);
	}

}
