package com.ssquare.facade.common.handler;

import com.ssquare.facade.common.handler.ssquare.Flow;
import com.ssquare.facade.common.handler.ssquare.ProgramCall;
import com.ssquare.facade.common.handler.ssquare.Screen;
import com.ssquare.facade.common.handler.ssquare.ScreenKey;
import com.ssquare.facade.common.handler.ssquare.Webservice;

/**
 * This is class for all function of ssquare
 * 
 * @author adirak-vdi
 *
 */
public final class SSquare {

	/** Don't create instance */
	private SSquare() {
	}

	/** instance of this class */
	public static final SSquare ssquare = new SSquare();
	
	/** instance of screen */
	public final Screen screen = Screen.screen;
	
	/** instance of screen key */
	public final ScreenKey screenKey = ScreenKey.screenKey;
	
	/** instance of webservice */
	public final Webservice webservice = Webservice.webservice;
	
	/** instance of programCall */
	public final ProgramCall programCall = ProgramCall.programCall;

	// Method to Implement
	// *****************************************************************
	/**
	 * To get flow function
	 * 
	 * @param singleton
	 * @return flow
	 */
	public Flow getFlow(long flowId) {
		return Flow.getFlow(flowId);
	}

	/**
	 * To get flow with graphModelXML
	 * 
	 * @param flowId
	 * @param graphModelXML
	 * @return flow
	 */
	public Flow getFlow(long flowId, String graphModelXML) {
		return Flow.getFlow(flowId, graphModelXML);
	}
	
	
	/**
	 * To get flow by flow name
	 * @param flowName
	 * @return
	 */
	public Flow getFlow(String flowName) {
		return Flow.getFlow(flowName);
	}
	// *****************************************************************
}
