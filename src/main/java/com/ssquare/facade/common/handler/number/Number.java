package com.ssquare.facade.common.handler.number;

import java.text.DecimalFormat;

public final class Number {

	/**
	 * Don't create instance
	 */
	private Number() {
	}

	/** instance of Number */
	public static final Number number = new Number();

	public int toInt(String str) {
		return Integer.parseInt(str);
	}

	public long toLong(String str) {
		return Long.parseLong(str);
	}

	public double toDouble(String str) {
		return Double.parseDouble(str);
	}

	public String toString(double num, String format) {
		DecimalFormat df = new DecimalFormat(format);
		return df.format(num);
	}

	public String toString(long num, String format) {
		DecimalFormat df = new DecimalFormat(format);
		return df.format(num);
	}

}
