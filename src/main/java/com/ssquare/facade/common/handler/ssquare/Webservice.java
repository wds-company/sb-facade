package com.ssquare.facade.common.handler.ssquare;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import blueprint.util.StringUtil;
import techberry.ewi.data.bean.entity.WebServiceBean;
import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.WSData;
import techberry.ewi.service.WebServiceService;
import techberry.ewi.webservice.util.WebServiceParser;

public class Webservice {

	/** Don't create new instance */
	private Webservice() {
	}

	/**
	 * Instance of this class
	 */
	public static final Webservice webservice = new Webservice();

	/** Instance of webservice service */
	public final WebServiceService wsService = new WebServiceService();

	/**
	 * Refresh input output from WSDL in config data (postgres)
	 * 
	 * @param cell
	 * @param wsName
	 * @param portType
	 * @param operation
	 * @throws IOException
	 */
	public void refreshInputOutputFromWSDL(EWICell cell, String wsName, String portType, String operation) throws IOException {

		if (!StringUtil.isBlank(portType) && !StringUtil.isBlank(operation)) {

			// Create parser
			WebServiceParser parser = createWsParser(wsName);

			try {

				Map<String, Object> inputfiels = parser.getSoapRequestTemplateFields(portType, operation);

				WSData data = cell.getWSData();
				data.addAllInputFields(inputfiels, true);

				Map<String, Object> outputfiels = parser.getSoapResponseTemplateFields(portType, operation);

				data.addAllOutputFields(outputfiels, true);

				// Log input data
				System.out.println("inputfiels=" + inputfiels);
				System.out.println("outputfiels=" + outputfiels);

			} catch (Exception e) {
				throw new IOException(e);
			}
		}

	}

	/**
	 * Create Webservice Parser
	 * 
	 * @param wsName
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked" })
	private WebServiceParser createWsParser(String wsName) throws IOException {

		List<WebServiceBean> list = wsService.findByName(wsName);
		WebServiceParser parser = null;
		if (list != null && list.size() > 0) {
			for (WebServiceBean bean : list) {
				String name = bean.getName();
				if (name.equals(wsName)) {
					WebServiceBean wsBean = list.get(0);
					byte[] bytes = wsBean.getWsdl();
					ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
					parser = new WebServiceParser(bais);
					break;
				}
			}

		}

		if (parser == null) {
			throw new IOException("Cannot create ws parser from : " + wsName);
		} else {
			return parser;
		}

	}

}
