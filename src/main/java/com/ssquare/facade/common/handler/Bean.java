package com.ssquare.facade.common.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public final class Bean {

	/** Don't create new instance */
	private Bean() {
	}

	/** instance of this class */
	public static final Bean bean = new Bean();

	/**
	 * Copy value from objectA to objectB
	 * 
	 * @param fromObj
	 * @param toObj
	 */
	public void copyValue(Object fromObj, Object toObj) {
		BeanUtils.copyProperties(fromObj, toObj);
	}

	/**
	 * Copy value from objectA to objectB
	 * @param fromObj
	 * @param toObj
	 * @param ingoreField
	 */
	public void copyValue(Object fromObj, Object toObj, String... ingoreField) {
		BeanUtils.copyProperties(fromObj, toObj, ingoreField);
	}
	
	/**
	 * Convert object to map
	 * @param object
	 * @return map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> toMap(Object object){
		
		Map<String, Object> result = new HashMap<String, Object>();
		ObjectMapper oMapper = new ObjectMapper();
		
		result =  oMapper.convertValue(object, result.getClass());
		
		return result;
	}
	
	/**
	 * Convert object to map
	 * @param object
	 * @return map
	 */
	public Object toObject(Map<String, Object> map, Object beanObj){
		
	
		ObjectMapper oMapper = new ObjectMapper();
		
		Object result = oMapper.convertValue(map, beanObj.getClass());
		
		return result;
	}

}
