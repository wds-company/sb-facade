package com.ssquare.facade.common.handler.ssquare;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import blueprint.util.StringUtil;
import techberry.ewi.data.bean.entity.ProgramCallBean;
import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.editor.PCData;
import techberry.ewi.programcall.data.EWIAS400Data;
import techberry.ewi.programcall.util.EWICopybookReader;
import techberry.ewi.service.ProgramCallService;
import techberry.ewi.util.ByteArrayConverter;

public class ProgramCall {

	/**
	 * Don't create new instance
	 */
	private ProgramCall() {
	}

	/** Instance of this class */
	public final static ProgramCall programCall = new ProgramCall();

	/** instance of program call service */
	private ProgramCallService service = new ProgramCallService();

	/**
	 * Get all program call name
	 * 
	 * @return list
	 */
	@SuppressWarnings({ "rawtypes" })
	public List<String> getProgramCallNames() {

		List<String> names = new ArrayList<String>();

		List list = service.findAll();
		if (list != null && list.size() > 0) {

			for (int i = 0; i < list.size(); i++) {
				ProgramCallBean bean = (ProgramCallBean) list.get(i);

				String name = bean.getName();
				names.add(name);
			}
		}

		return names;
	}

	/**
	 * Find program call by name
	 * 
	 * @param name
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private ProgramCallBean getProgramCallBean(String name) {
		if (!StringUtil.isBlank(name)) {
			List list = service.findByName(name);
			if (list != null && list.size() > 0) {

				for (int i = 0; i < list.size(); i++) {
					ProgramCallBean bean = (ProgramCallBean) list.get(i);

					String key = bean.getName();
					if (key.equals(name)) {
						return bean;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Refresh input output of programcall data from Copybook
	 * @param cell
	 * @param programCallName
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public void refreshInputOutputDataFromCopybook(EWICell cell, String programCallName) throws FileNotFoundException {

		ProgramCallBean bean = getProgramCallBean(programCallName);
		if (bean != null) {

			// Force to auto mapping
			boolean autoMapInput = true;
			boolean autoMapOutput = true;

			byte[] listBytes = bean.getCopybook();
			List<String> copybooks = null;
			if (listBytes != null) {
				copybooks = (List<String>) ByteArrayConverter.toObject(listBytes);
			}

			// Loading copybook data
			if (copybooks != null && !copybooks.isEmpty()) {

				// Clear old program call data
				PCData rootParams = cell.getPCData();
				rootParams.removeAllChildren();

				for (int i = 0; i < copybooks.size(); i++) {

					try {
						String copybook = copybooks.get(i);
						PCData pcData = getPCDataFromCopybook(copybook, autoMapInput, autoMapOutput);
						if (pcData != null) {
							while (pcData.getChildCount() > 0) {
								PCData childNode = (PCData) pcData.getChildAt(0);
								rootParams.add(childNode);
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}

			// No copybook data
			else {
				throw new FileNotFoundException("No copybook data for programCallConfigName = " + programCallName);
			}
		}

	}

	/**
	 * To get program call data from copybook string
	 * 
	 * @param copybook
	 * @return pcData
	 * @throws Exception
	 */
	private PCData getPCDataFromCopybook(String copybook, boolean autoGenIn, boolean autoGenOut) throws Exception {
		// System.out.println("Copybook = "+copybook);
		EWICopybookReader cbReader = new EWICopybookReader(copybook);
		EWIAS400Data nodeData = cbReader.readData();
		if (nodeData != null) {
			PCData pcData = new PCData(nodeData, autoGenIn, autoGenOut);
			return pcData;
		}
		return null;
	}

}
