package com.ssquare.facade.common.handler.ssquare;

import java.util.ArrayList;
import java.util.List;

import blueprint.util.StringUtil;
import techberry.ewi.data.bean.entity.FieldBean;
import techberry.ewi.data.bean.entity.ScreenBean;
import techberry.ewi.service.FieldMappingService;
import techberry.ewi.service.HostMappingService;
import techberry.ewi.service.ScreenMappingService;

public final class Screen {

	/** Don't create new instance */
	private Screen() {
	}

	/**
	 * Instance of this class
	 */
	public static final Screen screen = new Screen();

	/** instance of service */
	private ScreenMappingService screenService = new ScreenMappingService();
	private FieldMappingService fieldService = new FieldMappingService();
	private HostMappingService hostMappingService = new HostMappingService();

	/**
	 * Find Screen by name
	 * 
	 * @param screenName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ScreenBean getScreen(String screenName) {
		if (screenName != null && !screenName.trim().isEmpty()) {
			List<ScreenBean> list = screenService.findScreenByScreenName(screenName);
			if (list != null) {
				for (ScreenBean bean : list) {
					if (bean.getName().equals(screenName)) {
						return bean;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Find screen by id
	 * 
	 * @param id
	 * @return
	 */
	public ScreenBean getScreen(long id) {
		ScreenBean bean = new ScreenBean(id);
		return screenService.findById(bean);
	}

	/**
	 * Get field by screen
	 * 
	 * @param screen
	 * @param fieldName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public FieldBean getFieldByScreen(ScreenBean screen, String fieldName) {

		List<FieldBean> list = fieldService.findByScreen(screen);
		if (list != null) {
			for (FieldBean fb : list) {
				if (fb.getName().equals(fieldName)) {
					return fb;
				}
			}
		}

		return null;
	}

	/**
	 * Get field by screen
	 * 
	 * @param screenName
	 * @param fieldName
	 * @return
	 */
	public FieldBean getFieldByScreen(String screenName, String fieldName) {

		ScreenBean screen = getScreen(screenName);
		if (screen != null) {
			return getFieldByScreen(screen, fieldName);
		}

		return null;
	}

	/**
	 * List field by screen
	 * 
	 * @param screenName
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	public List<FieldBean> listFieldByScreen(String screenName) {

		ScreenBean screen = getScreen(screenName);
		if (screen != null) {
			List<FieldBean> list = fieldService.findByScreen(screen);
			List<FieldBean> filterList = new ArrayList<FieldBean>();
			if (list != null) {
				for (FieldBean fb : list) {
					String parentId = fb.getField1();
					if (StringUtil.isBlank(parentId) || fb.isCollection()) {
						filterList.add(fb);
					}
				}
			}
			return filterList;
		}

		return null;
	}

	/**
	 * Get children field of data list
	 * 
	 * @param screenName
	 * @param listName
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<FieldBean> listChildrenField(String screenName, String listName) {
		ScreenBean screen = getScreen(screenName);
		if (screen != null) {

			FieldBean fieldBean = hostMappingService.getMetaField(screen, listName);

			if (fieldBean != null) {
				List list = fieldService.getChildrenField(screen, fieldBean);
				return list;
			}

		}

		return null;

	}

}
