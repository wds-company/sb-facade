package com.ssquare.facade.common.handler.ssquare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.mxgraph.io.mxEWICodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.view.mxGraph;

import techberry.ewi.data.bean.entity.ScreenFlowBean;
import techberry.ewi.data.bean.entity.ScreenGroupBean;
import techberry.ewi.graph.editor.EWICell;
import techberry.ewi.graph.util.EWIGraphComponent;
import techberry.ewi.json.util.JsonObjectMapper;
import techberry.ewi.service.ScreenFlowService;
import techberry.ewi.service.ScreenGroupService;
import techberry.ewi.util.ByteArrayConverter;

/**
 * This class is handler about screen flow
 * 
 * @author adirak-vdi
 *
 */
public final class Flow {

	// Service Object
	// ++++++++++++++++++++++++++++++++++++++++++++

	private ScreenFlowService screenFlowService = new ScreenFlowService();
	private static ScreenGroupService screenGroupService = new ScreenGroupService();

	// ++++++++++++++++++++++++++++++++++++++++++++

	/** Flow data */
	private ScreenFlowBean screenFlow = null;
	private mxGraph graph = EWIGraphComponent.makeGraph();
	private boolean isUpdating = false;

	/**
	 * Get flow by serviceId
	 * 
	 * @param serviceName
	 */
	public static Flow getFlow(long serviceId) {
		return new Flow(serviceId);
	}

	/**
	 * Get flow by serviceId
	 * 
	 * @param serviceName
	 */
	public static Flow getFlow(String flowName) {
		return new Flow(flowName);
	}

	/**
	 * Get flow by serviceId and graphModel
	 * 
	 * @param serviceId
	 * @param graphModelXml
	 * @return
	 */
	public static Flow getFlow(long serviceId, String graphModelXml) {
		return new Flow(serviceId, graphModelXml);
	}

	/**
	 * Don't create new instance
	 * 
	 * @param serviceName
	 */
	private Flow(long serviceId) {
		this(serviceId, null);
	}

	/**
	 * new flow by name
	 * 
	 * @param flowName
	 * @throws Exception
	 */
	private Flow(String flowName) {
		this(getFlowIdByName(flowName), null);
	}

	/**
	 * 
	 * @param flowName
	 * @return id
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private static long getFlowIdByName(String flowName) {
		List<ScreenGroupBean> list = screenGroupService.findByGroupName(flowName);
		if (list != null && !list.isEmpty()) {
			ScreenGroupBean bean = list.get(0);
			return bean.getId();
		} else {
			throw new RuntimeException("Flow not found! flowName=" + flowName);
		}

	}

	/**
	 * Don't create new instance
	 * 
	 * @param serviceId
	 * @param graphModelXml
	 */
	private Flow(long serviceId, String graphModelXml) {
		readScreenFlow(serviceId, graphModelXml);
	}

	/**
	 * Initial screenFlow
	 * 
	 * @param serviceId
	 * @param graphModelXml
	 */
	private void readScreenFlow(long serviceId, String graphModelXml) {

		if (serviceId < 0) {
			serviceId = 0;
		}

		// Read ScreenFlow from DB
		screenFlow = screenFlowService.findById(serviceId);
		if (screenFlow == null) {
			// Make new ScreenFlow
			screenFlow = new ScreenFlowBean(serviceId);
			isUpdating = false;
		} else {
			isUpdating = true;
		}

		// graphModel data
		mxIGraphModel graphModel = null;

		// There are graphModel from frontend
		if (graphModelXml != null && graphModelXml.trim().length() > 10) {

			// Read GraphModel from XML
			Document document = mxXmlUtils.parseXml(graphModelXml);
			mxEWICodec codec = EWIGraphComponent.makeCodec(graphModelXml);
			codec.decode(document.getDocumentElement(), graph.getModel());

		}

		// Read data from graph
		else {
			byte[] flowData = screenFlow.getFlowData();
			if (flowData != null) {
				graphModel = (mxIGraphModel) ByteArrayConverter.toObject(flowData);
			}
		}

		// Refresh data to graph
		if (graphModel != null) {
			graph.setModel(graphModel);
		}

	}

	/**
	 * Get all edge in graph
	 * 
	 * @return
	 */
	public List<mxCell> getEdges() {

		mxIGraphModel model = graph.getModel();
		Object root = model.getRoot();
		Object[] children = graph.getChildCells(root);

		List<mxCell> cells = new ArrayList<mxCell>();

		// it has child node
		if (children.length > 0) {

			mxCell rootCell = (mxCell) children[0];
			Object[] allCells = graph.getChildCells(rootCell);

			// Find and set default cell
			for (int i = 0; i < allCells.length; i++) {
				Object cell = allCells[i];
				if ((cell instanceof EWICell) == false) {
					mxCell theCell = (mxCell) cell;
					try {
						int id = Integer.parseInt(theCell.getId());
						if (id > 1) {
							cells.add(theCell);
						}
					} catch (Exception e) {
					}

				}
			}

		}

		return cells;

	}

	/**
	 * Get all cells in this flow
	 * 
	 * @return list
	 */
	public List<EWICell> getEWICells() {

		mxIGraphModel model = graph.getModel();
		Object root = model.getRoot();
		Object[] children = graph.getChildCells(root);

		List<EWICell> cells = new ArrayList<EWICell>();

		// it has child node
		if (children.length > 0) {

			mxCell rootCell = (mxCell) children[0];
			Object[] allCells = graph.getChildCells(rootCell);

			// Find and set default cell
			for (int i = 0; i < allCells.length; i++) {
				Object cell = allCells[i];
				if (cell instanceof EWICell) {
					EWICell ewiCell = (EWICell) cell;
					cells.add(ewiCell);
				}
			}

		}

		return cells;
	}

	/**
	 * Get cell by nodeId and cell type
	 * 
	 * @param nodeId
	 * @param cellType
	 * @return
	 * @throws IOException
	 */
	public EWICell getEWICell(String nodeId, String cellType) throws IOException {

		List<EWICell> cells = getEWICells();

		if (cells != null && !cells.isEmpty()) {
			for (EWICell cell : cells) {
				String itemId = cell.getItemId();
				String itemType = cell.getCellType();
				// System.out.println(itemId + "==" + nodeId);
				// System.out.println(itemType + "==" + cellType);
				if (itemId.equals(nodeId) && itemType.equals(cellType)) {
					return cell;
				}
			}
		}

		throw new IOException("Cell not found! nodeId=" + nodeId + ", cellType=" + cellType);
	}

	/**
	 * Set get target of each cell
	 * 
	 * @param fromCell
	 * @return
	 */
	public List<EWICell> getTargetCell(EWICell fromCell) {
		List<EWICell> target = new ArrayList<EWICell>();

		Object[] edges = graph.getOutgoingEdges(fromCell);
		if (edges != null) {
			for (Object objCell : edges) {
				mxCell edge = (mxCell) objCell;
				mxICell targetCell = edge.getTarget();
				if (targetCell != null) {
					EWICell cell = (EWICell) targetCell;
					target.add(cell);
				} else {
					target.add(null);
				}
			}
		}

		return target;
	}

	/**
	 * Get target cell of edge
	 * 
	 * @param edge
	 * @return
	 */
	public EWICell getTargetCellFromEdge(mxCell edge) {

		EWICell ewiCell = null;

		mxICell targetCell = edge.getTarget();
		if (targetCell != null) {
			ewiCell = (EWICell) targetCell;
		}

		return ewiCell;
	}

	/**
	 * Get target cell from edge id
	 * 
	 * @param cell
	 * @param edgeId
	 * @return
	 */
	public EWICell getTargetCellFromEdgeId(EWICell cell, String edgeId) {
		List<mxCell> edges = getOutgoingEdges(cell);

		if (edges != null) {
			for (mxCell edge : edges) {
				String id = edge.getId();
				if (id.equals(edgeId)) {
					EWICell targetCell = (EWICell) edge.getTarget();
					return targetCell;
				}
			}
		}

		return null;
	}

	/**
	 * Get edge from edgeId
	 * 
	 * @param cell
	 * @return
	 */
	public mxCell getEdgeCellFromEdgeId(EWICell cell, String edgeId) {
		List<mxCell> edges = getOutgoingEdges(cell);

		if (edges != null) {
			for (mxCell edge : edges) {
				String id = edge.getId();
				if (id.equals(edgeId)) {
					return edge;
				}
			}
		}

		return null;
	}

	/**
	 * Get edge from target cell
	 * 
	 * @param cell
	 * @return
	 */
	public mxCell getEdgeFromTargetCell(EWICell cell) {

		List<mxCell> outgoing = getOutgoingEdges(cell);
		for (mxCell e : outgoing) {
			mxICell target = e.getTarget();
			if (target instanceof EWICell) {
				EWICell targetCell = (EWICell) target;
				if (targetCell.getItemId().equals(cell.getItemId())) {
					return e;
				}
			}
		}

		return null;
	}

	/**
	 * Get outgoing edge of cell
	 * 
	 * @return
	 */
	public List<mxCell> getOutgoingEdges(EWICell cell) {

		List<mxCell> cells = new ArrayList<mxCell>();

		Object[] outgoing = graph.getOutgoingEdges(cell);

		// it has child node
		if (outgoing != null) {

			for (Object obj : outgoing) {
				mxCell outCell = (mxCell) obj;
				cells.add(outCell);
			}

		}

		return cells;

	}

	/**
	 * Get edge between the cell
	 * 
	 * @param cell1
	 * @param cell2
	 * @return
	 */
	public mxCell getBetweenEdge(EWICell cell1, EWICell cell2) {

		Object[] edges1 = graph.getIncomingEdges(cell1);
		Object[] edges2 = graph.getOutgoingEdges(cell2);
		if (edges1 != null && edges2 != null) {
			for (Object obj1 : edges1) {							
				for(Object obj2 : edges2) {
					if(obj1 == obj2) {
						if(obj1 instanceof mxCell) {
							return (mxCell) obj1;
						}
					}
				}				
			}
		}
		
		edges1 = graph.getOutgoingEdges(cell1);
		edges2 = graph.getIncomingEdges(cell2);
		if (edges1 != null && edges2 != null) {
			for (Object obj1 : edges1) {							
				for(Object obj2 : edges2) {
					if(obj1 == obj2) {
						if(obj1 instanceof mxCell) {
							return (mxCell) obj1;
						}
					}
				}				
			}
		}
		
		
		return null;
	}

	/**
	 * Save ScreenFlow to database
	 */
	public void saveScreenFlow() throws IOException {

		// Get Model from graph
		mxIGraphModel graphModel = graph.getModel();
		byte[] flowData = ByteArrayConverter.toBytes(graphModel);

		boolean success = false;
		String msg = "Succesful";

		// Create new screen flow in database
		if (isUpdating) {
			screenFlow.setLastupdatedDate(new Date());
			screenFlow.setFlowData(flowData);
			success = screenFlowService.update(screenFlow);
			msg = "Update is fail! serviceId=" + screenFlow.getId();
		} else {
			screenFlow.setCreatedDate(new Date());
			screenFlow.setLastupdatedDate(new Date());
			screenFlow = screenFlowService.create(screenFlow);
			screenFlow.setFlowData(flowData);
			if (screenFlow != null) {
				success = true;
			} else {
				msg = "Create is fail! serviceId=" + screenFlow.getId();
			}
		}

		// Throw error if not success
		if (!success) {
			IOException ex = new IOException(msg);
			throw ex;
		}

	}

	/**
	 * Get flowId
	 * 
	 * @return
	 */
	public long getFlowId() {
		return screenFlow.getId();
	}

	/**
	 * Get graph model xml
	 * 
	 * @return
	 */
	public String getGraphModel() {

		mxIGraphModel graphModel = graph.getModel();

		// Save to mxe
		mxEWICodec codec = EWIGraphComponent.makeCodec();
		String xml = mxXmlUtils.getXml(codec.encode(graphModel));

		return xml;
	}

	/**
	 * Get promote model
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getPropmoteModel() {

		List<Map<String, Object>> promoteModel = null;
		try {

			// Read promote from database
			byte[] byteData = screenFlow.getPromoteFields();
			promoteModel = JsonObjectMapper.readToList(byteData);

		} catch (Exception e) {
			promoteModel = new ArrayList<Map<String, Object>>();
		}

		return promoteModel;

	}

	/**
	 * Save propmote data to database
	 * 
	 * @param promoteModel
	 * @throws Exception
	 */
	public void setPropmoteModel(List<Map<String, Object>> promoteModel) throws Exception {
		if (promoteModel == null) {
			promoteModel = new ArrayList<Map<String, Object>>();
		}
		byte[] byteData = JsonObjectMapper.toBytes(promoteModel);
		screenFlow.setPromoteFields(byteData);
	}

	/**
	 * Export graph model to file
	 * 
	 * @param path
	 * @throws IOException
	 */
	public void export(String path) throws IOException {

		mxIGraphModel graphModel = graph.getModel();

		// Save to mxe
		mxEWICodec codec = EWIGraphComponent.makeCodec();
		String xml = mxXmlUtils.getXml(codec.encode(graphModel));
		mxUtils.writeFile(xml, path);

		System.out.println("Exported.......");

	}
}
