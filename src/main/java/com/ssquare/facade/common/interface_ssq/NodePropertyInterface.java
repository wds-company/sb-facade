package com.ssquare.facade.common.interface_ssq;

import java.util.Map;

import com.ssquare.facade.common.handler.ssquare.data.NodeCell;

public interface NodePropertyInterface {

	/**
	 * To load property by nodeId
	 * 
	 * @param sc
	 * @param flow
	 * @param nodeId
	 * @return map
	 */
	public void loadProperty(NodeCell nodeCell) throws Exception;

	/**
	 * To save property by nodeProp, it is json data from frontend
	 * 
	 * @param sc
	 * @param flow
	 * @param nodeProp
	 * @return
	 */
	public void saveProperty(NodeCell nodeCell) throws Exception;

	/**
	 * To load pluginConfig data such as load excel on database node
	 * 
	 * @param nodeCell
	 * @return
	 * @throws Exception
	 */
	public Object loadPlugConfig(NodeCell nodeCell) throws Exception;

	/**
	 * To merge plugin config with current data such as webservice node, or
	 * programcall node
	 * 
	 * @param nodeCell
	 * @return
	 * @throws Exception
	 */
	public Object mergePlugConfig(NodeCell nodeCell, Object inputObj) throws Exception;

	/**
	 * To list field (only level 1 field)
	 * 
	 * @param nodeCell
	 * @return
	 */
	public Map<String, Object> listFieldName(NodeCell nodeCell);

	/**
	 * To list dataList (only level 1 field)
	 * 
	 * @param nodeCell
	 * @return
	 */
	public Map<String, Object> listDataListName(NodeCell nodeCell);

	/**
	 * To list sub field in dataList
	 * 
	 * @param nodeCell
	 * @return
	 */
	public Map<String, Object> listFieldNameInDataList(NodeCell nodeCell, String dataListName);
	

	
}
