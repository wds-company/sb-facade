package com.ssquare.facade.common.model;

import java.util.LinkedHashMap;
import java.util.Map;

public final class RequestData {

	public String version = "";
	public final Map<String, Object> data = new LinkedHashMap<String, Object>();
	public final Map<String, Object> connectors = new LinkedHashMap<String, Object>();

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data.clear();
		if (data != null) {
			this.data.putAll(data);
		}
	}

	public Map<String, Object> getConnectors() {
		return connectors;
	}

	public void setConnectors(Map<String, Object> connectors) {
		this.connectors.clear();
		if (connectors != null) {
			this.connectors.putAll(connectors);
		}
	}

}
