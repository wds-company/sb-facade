package com.ssquare.facade.common.model;

import java.util.LinkedHashMap;
import java.util.Map;

public final class ResponseData {

	public String version = "1.0";
	public final Map<String, Object> data = new LinkedHashMap<String, Object>();
	public final Map<String, Object> message = new LinkedHashMap<String, Object>();
	
	public ResponseData() {
		
		// Defualt value of system
		data.put("system", "SSQUARE");
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void addValue(String name, Object value) {
		this.data.put(name, value);
	}

	public void setData(Map<String, Object> data) {
		if (data != null) {
			this.data.putAll(data);
		}
	}

	public void clearData() {
		this.data.clear();
	}

	public Map<String, Object> getMessage() {
		if(message.isEmpty()) {
			setSuccessful();
		}
		return message;
	}

	public void setMessageData(String code, String msg) {
		if (code == null) {
			code = "";
		}
		if (msg == null) {
			msg = "";
		}
		message.put("code", code);
		message.put("description", msg);
	}

	public void setMessageDescription(String msg) {
		setMessageData((String) message.get("code"), msg);
	}

	public void setSuccessful() {
		setMessageData("200", "Successful");
	}

	public void setSuccessfulMessage(String msg) {
		setMessageData("200", msg);
	}

	public void setErrorMessage(String msg) {
		setMessageData("500", msg);
	}

	public void setResponseData(Map<String, Object> result, String code, String message) {
		this.setData(result);
		this.setMessageData(code, message);
	}

}
