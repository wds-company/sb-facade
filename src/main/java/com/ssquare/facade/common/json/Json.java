package com.ssquare.facade.common.json;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class Json {

	/**
	 * Don't create new instance
	 */
	private Json() {
	}

	/**
	 * instance of this class
	 */
	public static final Json json = new Json();
	
	
	/**
	 * Convert json file to map
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public Map<String, Object> toMap(File file) throws IOException{
		DocumentContext doc = JsonPath.parse(file);
		return doc.read("$");
	}
	
	/**
	 * Convert json string to map 
	 * @param jsonString
	 * @return
	 * @throws IOException
	 */
	public Map<String, Object> toMap(String jsonString) throws IOException{
		DocumentContext doc = JsonPath.parse(jsonString);
		return doc.read("$");
	}

}
