package test;

import static org.junit.Assert.fail;

import java.util.Date;

import org.junit.Test;

import com.ssquare.facade.common.ServiceCommon;
import com.ssquare.facade.common.handler.ssquare.Flow;

public class SSQFlowTest {

	@Test
	public void testSaveFlowToDB() {

		try {

			// Read XML Flow
			String xml = ServiceCommon.common.file.reader.readResourceToString("/testTemplate/sampleFlow.mxe");
			System.out.println(xml);

			long dummyFlowId = 100;
			Flow flow = ServiceCommon.common.ssquare.getFlow(dummyFlowId, xml);
			flow.saveScreenFlow();

		} catch (Exception e) {
			fail("Fail to save Flow : " + e.getMessage());
		}

	}
	
	@Test
	public void testTimeStamp() {
		
		Long time = new Date().getTime();
		
		String timeHex = Long.toHexString(time);
		
		System.out.println(time);
		System.out.println(timeHex.toUpperCase());
	}

}
